package com.hendisantika.jpahibernatecompositeprimarykeyexamplewithmysql;

import com.hendisantika.jpahibernatecompositeprimarykeyexamplewithmysql.entity.Book;
import com.hendisantika.jpahibernatecompositeprimarykeyexamplewithmysql.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class JpaHibernateCompositePrimaryKeyExampleWithMysqlApplication implements CommandLineRunner {
    private final BookRepository bookRepository;

    @Override
    public void run(String... args) {
        bookRepository.save(Book.builder()
                .name("Uzumaki Naruto")
                .publishedDate(new Date())
                .build());

        log.info("My books: " + bookRepository.findAll());
    }
    public static void main(String[] args) {
        SpringApplication.run(JpaHibernateCompositePrimaryKeyExampleWithMysqlApplication.class, args);
    }

}

