package com.hendisantika.jpahibernatecompositeprimarykeyexamplewithmysql.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : jpa-hibernate-composite-primary-key-example-with-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-10
 * Time: 09:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@IdClass(Book.IdClass.class)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    @Id
    private String name;

    @Id
    private Date publishedDate;

    @Builder.Default
    private Date updatedDate = new Date();

    @Data
    public static class IdClass implements Serializable {
        private String name;
        private Date publishedDate;
    }


}
