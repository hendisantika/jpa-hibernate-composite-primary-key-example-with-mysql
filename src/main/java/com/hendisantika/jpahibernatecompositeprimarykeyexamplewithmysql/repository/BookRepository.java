package com.hendisantika.jpahibernatecompositeprimarykeyexamplewithmysql.repository;

import com.hendisantika.jpahibernatecompositeprimarykeyexamplewithmysql.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : jpa-hibernate-composite-primary-key-example-with-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-10
 * Time: 09:02
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Book.IdClass> {
}