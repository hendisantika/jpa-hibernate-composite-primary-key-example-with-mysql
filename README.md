# JPA/Hibernate Composite Primary Key Entity Mapping Example with MySQL

This post walks you through the steps of mapping a composite primary key entity with Spring Data JPA, Lombok, MySQL and Docker Compose.

#### What you'll need
* Docker Compose
* Stack
* MySQL
* Spring Data JPA
* Spring Boot
* Java
* Lombok

#### Run this project by this command :
1. Clone this project : `git clone https://gitlab.com/hendisantika/jpa-hibernate-composite-primary-key-example-with-mysql.git`
2. Go to its folder : `jpa-hibernate-composite-primary-key-example-with-mysql`
3. Run the app : `docker-compose up`

![Spring Boot Console](img/console.png "Spring Boot Console")

Let's check the data on MySQL database. Access to MySQL Server docker container by issuing below bash command

`docker exec -it some-mysql` bash`

![MySQL Console](img/mysql1.png "MySQL Console")

Type mysql -p to access to MySQL local database server.

![MySQL Console](img/mysql2.png "MySQL Console")
